﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameGrid : MonoBehaviour {

    /// <summary>
    /// Height and Width of the Game of Life field
    /// </summary>
	[SerializeField] int m_gridsize = 0;

    /// <summary>
    /// Reference to the Cell Object that will be spawned
    /// </summary>
	[SerializeField] GameObject m_cellPrefab;

    /// <summary>
    /// List of all generated cells ( needed for reference )
    /// </summary>
	List<GameCell> m_allCells = new List<GameCell>();

    /// <summary>
    /// Time that passed between each simulation step
    /// </summary>
	[SerializeField] float m_simulationTimeStep;

    /// <summary>
    /// Timestamp for the next simulation step
    /// </summary>
	float m_simulationTimeStamp;

    /// <summary>
    /// if activated the simulation runs continously
    /// </summary>
	bool m_playSimulation;

    /// <summary>
    /// Untiy start function to initatlize the game cells
    /// </summary>
	void Start () {

        ///place camera in the center
		Vector3 cameraPostion = Camera.main.transform.position;
		cameraPostion.y = m_gridsize + 3;
		Camera.main.transform.position = cameraPostion;

        ///buffer to memorize cells
		GameCell cell_upperLeft = null;
		GameCell cell_lowerLeft = null;
		GameCell cell_upper = null;
		GameCell cell_left = null;
        ///buffer to memorize all the cells to the left
		List<GameCell> leftCells = new List<GameCell>();
		List<GameCell> currentCells = new List<GameCell>();

		GameCell gameCell = null;

        ///iterate trought a 2 dimensional field with two loops
		for (int index_x = 0; index_x < m_gridsize; index_x++) 
		{
			for (int index_y = 0; index_y < m_gridsize; index_y++) 
			{
                ///create the new cell and put it into the correct grid position
				GameObject newCell = GameObject.Instantiate (m_cellPrefab);
				newCell.name = "Cell_X_" + index_x + "_Y_" + index_y;
				float halfOfGridsize = ((float)m_gridsize) / 2f;
				newCell.transform.position = new Vector3(0.5f + index_x - halfOfGridsize,0f,0.5f + index_y - halfOfGridsize);

                //save old cells to the buffer
				if(leftCells.Count != 0){ cell_left = leftCells [index_y]; }

				gameCell = newCell.GetComponent<GameCell> ();
				m_allCells.Add (gameCell);
                ///adding old saved cells as neighbours
				gameCell.AddNeigbour (cell_upperLeft,true);
				gameCell.AddNeigbour (cell_lowerLeft,true);
				gameCell.AddNeigbour (cell_upper,true);
				gameCell.AddNeigbour (cell_left,true);
					
                ///updating old cells
				cell_upper = gameCell;
				cell_upperLeft = cell_left;

				if (leftCells.Count != 0 && index_y < m_gridsize - 2) {
					cell_lowerLeft = leftCells [index_y + 2];
				}
                
				currentCells.Add (gameCell);
			}

			cell_upper = null;
			cell_upperLeft = null;
			cell_lowerLeft = null;

			leftCells.Clear ();
			leftCells.InsertRange (0,currentCells);
			currentCells.Clear ();

		}

	}
	
	// Update is called once per frame
	void Update () {

		///check for mouse input
		if (Input.GetMouseButtonDown(0))
		{
			GameObject underMouseObject = ReturnClickedObject();

			///get cell under mouse
			if (underMouseObject != null)
			{
				GameCell cell = underMouseObject.GetComponent<GameCell> ();

				if (cell != null) 
				{
					cell.OnMouseClick ();
				}  
			}
		}

        ///if simulation is activated simulate the game rules every time intervall
		if (m_playSimulation) {

			if (Time.time > m_simulationTimeStamp + m_simulationTimeStep) {
			
				Simulate ();
				m_simulationTimeStamp = Time.time;
			
			}
		}
	}

	/// <summary>
	/// Returns the clicked object under the mouse position
	/// </summary>
	/// <returns>The clicked object.</returns>
	GameObject ReturnClickedObject()
	{
		GameObject target = null;

		RaycastHit hit;

		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		Physics.Raycast (ray,out hit, 10000);

		if (hit.collider != null)
		{
			target = hit.collider.gameObject;
		}

		return target;
	} 

    /// <summary>
    /// Play button is pressed
    /// </summary>
	public void PlaySimulation()
	{
		m_playSimulation = true;
	}

    /// <summary>
    /// Stop Button is Pressed
    /// </summary>
	public void StopSimulation()
	{
		m_playSimulation = false;
	}

    /// <summary>
    /// Simulate Button is pressed or Timestamp is reached - Simulate all cells
    /// </summary>
	public void Simulate()
	{
        ///fisrt apply the rules for ALL cells
		foreach(GameCell cell in m_allCells)
		{
            GameRules.CalculateGameOfLife(cell);
		}

        ///afterwards set the state for all cells 
		foreach(GameCell cell in m_allCells)
		{
            GameRules.SetCalculation(cell);
		}
	}

    public void ClearGrid()
    {
        ///fisrt apply the rules for ALL cells
		foreach (GameCell cell in m_allCells)
        {
            cell.SetAlive(false);
        }

    }
}
