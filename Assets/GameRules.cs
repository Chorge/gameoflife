﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRules {

    /// <summary>
    /// Calculate the Rules for Game of Life on a single Cell
    /// </summary>
    /// <param name="cell"></param>
    static public void CalculateGameOfLife(GameCell cell)
    {
        int aliveNeigbours = 0;

        ///count all alive neigbours
        foreach (GameCell neigbourcell in cell.GetNeigbourList())
        {
            if (neigbourcell.IsAlive())
            {
                aliveNeigbours++;
            }
        }

        ///apply the game of life rules
        if (cell.IsAlive())
        {
            ///the cell survives they have 2 or 3 neigbours
            cell.SetAliveNextRound( aliveNeigbours == 2 || aliveNeigbours == 3 );
        }
        else
        {
            ///the cell is born again with 3 neigbours
            cell.SetAliveNextRound( aliveNeigbours == 3 ) ;
        }
    }

    /// <summary>
    /// Set the state of the cell to the calculated result
    /// </summary>
    /// <param name="cell"></param>
    static public void SetCalculation(GameCell cell)
    {
        cell.SetAlive(cell.IsAliveNextRound());
    }
}
