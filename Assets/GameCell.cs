﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCell : MonoBehaviour {
    
    /// <summary>
    /// List of all neighbouring cells
    /// </summary>
	public List<GameCell> m_neigbours;

    /// <summary>
    /// the alive state of the cell
    /// </summary>
	bool m_isAlive = true;

    /// <summary>
    /// in the next round, will the cell be alive?
    /// </summary>
	bool m_aliveNextRound = true;

    /// <summary>
    /// the mesh renderer of the cell
    /// </summary>
	MeshRenderer m_myMeshRenderer;

	void Start()
	{
        ///save the meshrenderer into a member variable
		m_myMeshRenderer = GetComponent<MeshRenderer> ();
	}

    /// <summary>
    /// If the Cell is set to not alive hide the mesh
    /// </summary>
    /// <param name="alive"></param>
	public void SetAlive(bool alive)
	{
		m_isAlive = alive;
		m_myMeshRenderer.enabled = alive;
	}

    /// <summary>
    /// Is the cell in the alive state?
    /// </summary>
    /// <returns></returns>
	public bool IsAlive()
	{
		return m_isAlive;
	}

    /// <summary>
    /// Will the cell be in the alive state next round?
    /// </summary>
    /// <returns></returns>
    public bool IsAliveNextRound()
    {
        return m_aliveNextRound;
    }

    /// <summary>
    /// Set if the cell will be in the alive state next round
    /// </summary>
    /// <param name="set"></param>
    public void SetAliveNextRound(bool set)
    {
        m_aliveNextRound = set;
    }

    /// <summary>
    /// Return the list of neighbours
    /// </summary>
    /// <returns></returns>
    public List<GameCell> GetNeigbourList()
    {
        return m_neigbours;
    }

    //When clicked on a Cell , toogle the state
    public void OnMouseClick()
	{
		SetAlive ( !m_isAlive );

		Debug.Log("Cell " + gameObject.name + " was clicked by mouse!");
	}


    /// <summary>
    /// Adds a new neighbour to the cell. Also the neigbour cell can add this cell at its neigbour 
    /// </summary>
    /// <param name="cell"></param>
    /// <param name="addAlsoToNeigbour"></param>
	public void AddNeigbour(GameCell cell, bool addAlsoToNeigbour)
	{
		if (cell != null) {
  
			if (!m_neigbours.Contains (cell)) {
				m_neigbours.Add (cell);
			}

			if (addAlsoToNeigbour) {
				cell.AddNeigbour (this,false);
			}
		}
	}

	
}
